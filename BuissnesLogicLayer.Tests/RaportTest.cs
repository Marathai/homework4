﻿using System;
using System.Collections.Generic;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using HomeworkPart2;
using HomeworkPart2.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BuissnesLogicLayer.Tests
{
    [TestClass]
    public class RaportTest
    {
        [TestMethod]
        public void TestReport1()
        {
            var pesel1 = new Pesel("12345123450");
            var student1 = new StudentDto();
            student1.Pesel = pesel1;
            student1.Id = 1;
            student1.Birthday = DateTime.Parse("1/1/1");
            student1.Name = "Janusz";
            student1.Surname = "Nowak";
            student1.StudentGender = StudentDto.Gender.Male;

            var pesel2 = new Pesel("12345123451");
            var student2 = new StudentDto();
            student2.Pesel = pesel2;
            student2.Id = 2;
            student2.Birthday = DateTime.Parse("1/1/1");
            student2.Name = "Halyna";
            student2.Surname = "Nowak";
            student2.StudentGender = StudentDto.Gender.Female;

            var pesel3 = new Pesel("12345612345");
            var student3 = new StudentDto();
            student3.Pesel = pesel3;
            student3.Id = 3;
            student3.Birthday = DateTime.Parse("1/1/2");
            student3.Name = "Brajan";
            student3.Surname = "Pasek";
            student3.StudentGender = StudentDto.Gender.Other;

            var pesel4 = new Pesel("12345654321");
            var student4 = new StudentDto();
            student4.Pesel = pesel4;
            student4.Id = 4;
            student4.Birthday = DateTime.Parse("1/2/2");
            student4.Name = "Karyna";
            student4.Surname = "Ajfon";
            student4.StudentGender = StudentDto.Gender.Female;

            var homework1 = new HomeworkDto();
            homework1.Id = 1;
            homework1.RequiredPoints = 12;
            homework1.EarnedPoints = new Dictionary<StudentDto, int> { { student1, 2 }, { student2, 11 } };

            var homework2 = new HomeworkDto();
            homework2.Id = 2;
            homework2.RequiredPoints = 12;
            homework2.EarnedPoints = new Dictionary<StudentDto, int> { { student1, 5 }, { student2, 10 } };

            var homework3 = new HomeworkDto();
            homework3.Id = 3;
            homework3.RequiredPoints = 15;
            homework3.EarnedPoints = new Dictionary<StudentDto, int> { { student1, 1 }, { student2, 12 } };

            var classDay1 = new ClassDayDto();
            classDay1.Id = 1;
            classDay1.ClassDate = DateTime.Parse("2/3/2");
            classDay1.Presense = new Dictionary<StudentDto, PresenceDto> { { student1, new PresenceDto(classDay1,student1,true) }, { student2, new PresenceDto(classDay1, student2, true) } };

            var classDay2 = new ClassDayDto();
            classDay2.Id = 2;
            classDay2.ClassDate = DateTime.Parse("2/4/2");
            classDay2.Presense = new Dictionary<StudentDto, PresenceDto> { { student1, new PresenceDto(classDay2, student1, false) }, { student2, new PresenceDto(classDay2, student2, true) } };

            var classDay3 = new ClassDayDto();
            classDay3.Id = 1;
            classDay3.ClassDate = DateTime.Parse("2/3/2");
            classDay3.Presense = new Dictionary<StudentDto, PresenceDto> { { student1,new PresenceDto(classDay3, student1, false) }, { student2, new PresenceDto(classDay3, student2, true) } };

            var course = new CourseDto();
            course.Id = 1;
            course.AmountOfCourseDays = 12;
            course.AmountOfStudents = 2;
            course.CourseName = "Kurs na superbohatera";
            course.CourseStartDate = DateTime.Parse("2/1/2");
            course.Students = new List<StudentDto> { student1, student2 };
            course.HomeworkThreshold = 50;
            course.PresenceThreshold = 50;
            course.Homeworks = new List<HomeworkDto> { homework1, homework2, homework3 };
            course.ClassDays = new List<ClassDayDto> { classDay1, classDay2, classDay3 };



            var report = new RaportForm();
            var result = report.ShowRaport(course);
            var expectedResult = "Kurs na superbohatera\n02.01.2002 00:00:00\n50\n50\nLista obecności\n12345123450 Janusz Nowak 1/3 (33,33333%) - Niezaliczone\n12345123451 Halyna Nowak 3/3 (100%) - Zaliczone\nRaport z prac domowych\n12345123450 Janusz Nowak 8/39 (20,51282%) - Niezaliczone\n12345123451 Halyna Nowak 33/39 (84,61539%) - Zaliczone\n";

            Assert.AreEqual(result, expectedResult);

        }

        [TestMethod]
        public void TestReport2()
        {
            var pesel1 = new Pesel("12345123450");
            var student1 = new StudentDto();
            student1.Pesel = pesel1;
            student1.Id = 1;
            student1.Birthday = DateTime.Parse("1/1/1");
            student1.Name = "Janusz";
            student1.Surname = "Nowak";
            student1.StudentGender = StudentDto.Gender.Male;

            var pesel2 = new Pesel("12345123451");
            var student2 = new StudentDto();
            student2.Pesel = pesel2;
            student2.Id = 2;
            student2.Birthday = DateTime.Parse("1/1/1");
            student2.Name = "Halyna";
            student2.Surname = "Nowak";
            student2.StudentGender = StudentDto.Gender.Female;

            var course = new CourseDto();
            course.Id = 1;
            course.AmountOfCourseDays = 12;
            course.AmountOfStudents = 2;
            course.CourseName = "Kurs na superbohatera";
            course.CourseStartDate = DateTime.Parse("2/1/2");
            course.Students = new List<StudentDto> { student1, student2 };
            course.HomeworkThreshold = 50;
            course.PresenceThreshold = 50;
            course.Homeworks = new List<HomeworkDto> { };
            course.ClassDays = new List<ClassDayDto> {  };

            var report = new RaportForm();
            var result = report.ShowRaport(course);
            var expectedResult =
                "Kurs na superbohatera\n02.01.2002 00:00:00\n50\n50\nLista obecności\n12345123450 Janusz Nowak 0/0 (0%) - Niezaliczone\n12345123451 Halyna Nowak 0/0 (0%) - Niezaliczone\nRaport z prac domowych\n12345123450 Janusz Nowak 0/0 (0%) - Niezaliczone\n12345123451 Halyna Nowak 0/0 (0%) - Niezaliczone\n";
            Assert.AreEqual(result, expectedResult);

        }
    }
}
