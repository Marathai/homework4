﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework1.Mappers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using DataLayer.Models;
using Moq;

namespace Homework1.Mappers.Tests
{
    [TestClass()]
    public class FromEntityToDtoMapperTests
    {
        [TestMethod()]
        public void EntityModelToStudentDtoSimpleTest()
        {
            var student = new Student();
            student.Pesel = "12345123450";
            student.Id = 3;
            student.Birthday = DateTime.Parse("1/1/1");
            student.Name = "Janusz";
            student.Surname = "Nowak";
            student.StudentGender = Student.Gender.Male;

            var expectedEntity = new StudentDto();
            var pesel = new Pesel("12345123450");
            expectedEntity.Pesel = pesel;
            expectedEntity.Id = 3;
            expectedEntity.Birthday = DateTime.Parse("1/1/1");
            expectedEntity.Name = "Janusz";
            expectedEntity.Surname = "Nowak";
            expectedEntity.StudentGender = StudentDto.Gender.Male;

            var result = FromEntityToDtoMapper.EntityModelToStudentDto(student);

            Assert.AreEqual(expectedEntity.Id, result.Id);
            Assert.AreEqual(expectedEntity.Birthday, result.Birthday);
            Assert.AreEqual(expectedEntity.StudentGender, result.StudentGender);
            Assert.AreEqual(expectedEntity.Name, result.Name);
            Assert.AreEqual(expectedEntity.Surname, result.Surname);
            Assert.AreEqual(expectedEntity.Pesel._peselNumber, result.Pesel._peselNumber);
        }

        [TestMethod()]
        public void EntityModelToStudentDtoSimpleReturnsNullTest()
        {
            Assert.AreEqual(null, FromEntityToDtoMapper.EntityModelToStudentDtoSimple(null));
        }

        [TestMethod()]
        //[ExpectedException(typeof(ArgumentNullException))]
        public void EntityModelToStudentDtoTest()
        {
            var course = new Course();
            course.Id = 1;
            course.AmountOfCourseDays = 12;
            course.AmountOfStudents = 5;
            course.CourseName = "Kurs na superbohatera";
            course.CourseStartDate = DateTime.Parse("1/1/1");

            var student = new Student();
            student.Pesel = "12345123451";
            student.Courses.Add(course);

            var expectedStudentDto = new StudentDto();
            expectedStudentDto.Id = 1;
            expectedStudentDto.Pesel = new Pesel("12345123451");
            expectedStudentDto.Courses.Add(
                FromEntityToDtoMapper.EntityToCourseDtoSimple(course)
            );
            var result = FromEntityToDtoMapper.EntityModelToStudentDto(student);
            Assert.IsTrue(result.Courses[0].CourseName == expectedStudentDto.Courses[0].CourseName);
        }

        private Student getStudent1()
        {
            var student = new Student();
            student.Pesel = "12345123450";
            student.Id = 3;
            student.Birthday = DateTime.Parse("1/1/1");
            student.Name = "Janusz";
            student.Surname = "Nowak";
            student.StudentGender = Student.Gender.Male;
            return student;
        }

        [TestMethod()]
        public void EntityModelToStudentDtoReturnsNullTest()
        {
            Assert.AreEqual(null, FromEntityToDtoMapper.EntityModelToStudentDto(null));
        }



        [TestMethod()]
        public void EntityToCourseDtoSimpleTest()
        {
            var course = new Course();
            course.Id = 1;
            course.AmountOfCourseDays = 12;
            course.AmountOfStudents = 5;
            course.TrainerName = "Trener";
            course.CourseStartDate = DateTime.Parse("1/1/1");
            course.CourseName = "Kurs na superbohatera";
            course.CourseStartDate = DateTime.Parse("1/1/1");

            var expectedEntity = new CourseDto();
            expectedEntity.Id = 1;
            expectedEntity.AmountOfCourseDays = 12;
            expectedEntity.AmountOfStudents = 5;
            expectedEntity.TrainerName = "Trener";
            expectedEntity.CourseStartDate = DateTime.Parse("1/1/1");
            expectedEntity.CourseName = "Kurs na superbohatera";
            expectedEntity.CourseStartDate = DateTime.Parse("1/1/1");

            var result = FromEntityToDtoMapper.EntityToCourseDtoSimple(course);
            Assert.AreEqual(expectedEntity.Id, result.Id);
            Assert.AreEqual(expectedEntity.AmountOfCourseDays, result.AmountOfCourseDays);
            Assert.AreEqual(expectedEntity.AmountOfStudents, result.AmountOfStudents);
            Assert.AreEqual(expectedEntity.CourseName, result.CourseName);
            Assert.AreEqual(expectedEntity.CourseStartDate, result.CourseStartDate);
        }

        [TestMethod()]
        public void EntityToCourseDtoSimpleReturnsNullTest()
        {
            Assert.AreEqual(null, FromEntityToDtoMapper.EntityToCourseDtoSimple(null));
        }

        [TestMethod()]
        public void EntityToCourseDtoTest()
        {
            var student = new Student();
            student.Pesel = "12345123450";
            student.Id = 3;
            student.Birthday = DateTime.Parse("1/1/1");
            student.Name = "Janusz";
            student.Surname = "Nowak";
            student.StudentGender = Student.Gender.Male;

            var homework = new Homework();
            homework.Id = 1;
            homework.RequiredPoints = 12;
            homework.EarnedPoints.Add(new EarnedPoints(homework,student,5));

            var classDay = new ClassDay();
            classDay.Id = 2;
            classDay.ClassDate = DateTime.Parse("3/2/2");
            classDay.Presense.Add(new Presence(classDay,student,true));

            var studentExpectedEntity = new StudentDto();
            studentExpectedEntity.Pesel = new Pesel("12345123450");
            studentExpectedEntity.Id = 3;
            studentExpectedEntity.Birthday = DateTime.Parse("1/1/1");
            studentExpectedEntity.Name = "Janusz";
            studentExpectedEntity.Surname = "Nowak";
            studentExpectedEntity.StudentGender = StudentDto.Gender.Male;

            var homeworkExpectedEntity = new HomeworkDto();
            homeworkExpectedEntity.Id = 1;
            homeworkExpectedEntity.RequiredPoints = 12;
            homeworkExpectedEntity.EarnedPoints = new Dictionary<StudentDto, int>();
            homeworkExpectedEntity.EarnedPoints.Add(studentExpectedEntity, 5);

            var classDayExpectedEntity = new ClassDayDto();
            classDayExpectedEntity.Id = 2;
            classDayExpectedEntity.ClassDate = DateTime.Parse("3/2/2");
            classDayExpectedEntity.Presense.Add(studentExpectedEntity, new PresenceDto(classDayExpectedEntity,studentExpectedEntity,true));

            var course = new Course();
            course.Students = new List<Student>();
            course.Homeworks = new List<Homework>();
            course.ClassDays = new List<ClassDay>();
            course.Students.Add(student);
            course.Homeworks.Add(homework);
            course.ClassDays.Add(classDay);

            var studentExpectedEntityList = new List<StudentDto>();
            var homeworktExpectedEntityList = new List<HomeworkDto>();
            var classDayExpectedEntityList = new List<ClassDayDto>();
            studentExpectedEntityList.Add(studentExpectedEntity);
            homeworktExpectedEntityList.Add(homeworkExpectedEntity);
            classDayExpectedEntityList.Add(classDayExpectedEntity);

            var result = FromEntityToDtoMapper.EntityToCourseDto(course);
            Assert.AreEqual(studentExpectedEntityList.Count, result.Students.Count);
            Assert.AreEqual(homeworktExpectedEntityList.Count, result.Homeworks.Count);
            Assert.AreEqual(classDayExpectedEntityList.Count, result.ClassDays.Count);
        }

        [TestMethod()]
        public void EntityToCourseDtoReturnsNullTest()
        {
            Assert.AreEqual(null, FromEntityToDtoMapper.EntityToCourseDto(null));
        }

        private Homework getHomework1()
        {
            var homework = new Homework();
            homework.Id = 1;
            homework.RequiredPoints = 12;
            return homework;
        }

        private ClassDay getClassDay1(string date = "3/2/2")
        {
            var classDay = new ClassDay();
            classDay.Id = 2;
            classDay.ClassDate = DateTime.Parse("3/2/2");
            return classDay;
        }

        [TestMethod()]
        public void EntityToCourseDtoTest1()
        {
            var course = new Course();
            var s1 = getStudent1();
            var h1 = getHomework1();
            var cd1 = getClassDay1();
            h1.EarnedPoints.Add(new EarnedPoints(h1,s1,5));
            course.Students = new List<Student> { s1 };
            course.Homeworks = new List<Homework> { h1 };
            course.ClassDays = new List<ClassDay> { cd1 };

            var dto = FromEntityToDtoMapper.EntityToCourseDto(course);
            Assert.AreEqual(dto.Students.Count, course.Students.Count);
            Assert.AreEqual(dto.Homeworks.Count, course.Homeworks.Count);
            Assert.AreEqual(dto.ClassDays.Count, course.ClassDays.Count);
        }

        [TestMethod()]
        public void EntityToHomeworkDtoTestWithNull()
        {
            Assert.IsNull(FromEntityToDtoMapper.EntityToHomeworkDto(null));
        }

        [TestMethod()]
        public void EntityToClassDayDtoTestWithNull()
        {
            Assert.IsNull(FromEntityToDtoMapper.EntityToClassDayDto(null));
        }
    }
}