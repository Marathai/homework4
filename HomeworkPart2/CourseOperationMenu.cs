﻿using System;
using System.Collections.Generic;
using System.IO;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using Homework1;
using Homework1.Services;
using HomeworkPart2.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HomeworkPart2
{
    public class CourseOperationMenu
    {
        private static bool _inLoop = true;
        private static CourseDto course = null;
        

        public delegate void DelegateEditCourseOption();
        public delegate void DelegateUSKOption();
        public delegate void DelegateDSOption();
        public delegate void DelegateROperation();
        public delegate void DelegateDPDOperation();
        public delegate void DelegateDDZOption();
        public delegate void DelegateRepot();
        public delegate void DelegateExit();

        public Dictionary<string, Delegate> MenuCourseDict = new Dictionary<string, Delegate>
        {
            { "DS", new DelegateDSOption(DSOption)},
            { "DDZ", new DelegateDDZOption(DDZOption)},
            { "USK", new DelegateUSKOption(USKOption)},
            { "E", new DelegateEditCourseOption(EditCourseOption)},
            { "DPD", new DelegateDPDOperation(DPDOperation)},
            { "R", new DelegateRepot(ROperation)},
            { "exit", new DelegateExit(Exit)}
        };

        public CourseOperationMenu(CourseDto course)
        {
            this.course = course;
        }

        public void ShowMenu()
        {
            while (_inLoop)
            {
                Console.WriteLine(
                    "Co chcesz zrobić? " +
                    "\n " +
                    "\nDodaj studenta - DS; " +
                    "\nUsun studenta z kursu - USK " +
                    "\nDodać dzień zajec - DDZ; " +
                    "\nDodać prace domową - DPD; " +
                    "\nWypisać raport - R; " +
                    "\nEdytuj dane kursu - E " +
                    "\nZakończyć - exit");

                var operations = Console.ReadLine();

                foreach (var option in MenuCourseDict)
                {
                    if (operations == option.Key)
                        MenuCourseDict[operations].DynamicInvoke();
                    else
                        Console.WriteLine("Bledna komenda menu");
                }
            }
        }

        private static void Exit()
        {
            _inLoop = false;
        }

        private static void EditCourseOption()
        {
            var service = new CourseService();

            Console.WriteLine("Podaj nowa nazwe");
            var input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.CourseName = input;
            }

            Console.WriteLine("Podaj nowe dane prowadzacego");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.TrainerName = input;
            }

            Console.WriteLine("Podaj nowy prog punktowy dla pracy domowej");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.HomeworkThreshold = Int32.Parse(input);
            }

            Console.WriteLine("Podaj nowy prog punktowy dla obecnosci");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.PresenceThreshold = Int32.Parse(input);
            }
            service.UpdateCourse(course);
        }

        private static void USKOption()
        {
            Pesel p = ConsoleInput.GetPesel("Podaj pesel studenta");
            StudentService ss = new StudentService();
            StudentDto studentDto = ss.GetStudentDtoByPesel(p);

            if (studentDto != null)
            {
                ss.RemoveStudentFromCourse(studentDto, course);
            }
            else
            {
                Console.WriteLine("Nie znalazlem takiego studenta");
            }
        }

        private static void DSOption()
        {
            Pesel p = ConsoleInput.GetPesel("Podaj pesel studenta");
            StudentService ss = new StudentService();
            StudentDto studentDto = ss.GetStudentDtoByPesel(p);
            
            if (studentDto != null)
            {
                
                course.AddStudentToCourse(studentDto);

            }
            else
            {
                Console.WriteLine("Nie znalazlem takiego studenta");
            }
        }

        private static void ROperation()
        {
            var raport = new RaportForm();
            string raportResult = raport.ShowRaport(course);
            Console.WriteLine(raportResult);
            string operation = ConsoleInput.GetYesNo("Czy chcesz zapisać do pliku JSON? (tak/nie)");
            if (operation == "tak")
            {
                raport.RaportCreated+= EventInfo;
                JObject jObject = JObject.Parse(raportResult);
                Console.WriteLine("Podaj nazwe dokumentu");
                operation = Console.ReadLine();

                using (StreamWriter file = File.CreateText(@"\"+operation+".json"))
                using(JsonTextWriter writer = new JsonTextWriter(file))
                {
                    jObject.WriteTo(writer);
                }
            }
        }

        private static void DPDOperation()
        {
            //string operations;
            var reqPoints = ConsoleInput.GetInt("Podaj maksymalna ilość punktów do uzyskania");
            HomeworkDto homework = new HomeworkDto(reqPoints);

            foreach (var student in course.Students)
            {
                int points = ConsoleInput.GetInt("Podaj punkty uzyskane przez " + student.Name + " " + student.Surname);
                homework.EarnedPoints.Add(student, points);
            }
            course.AddHomework(homework);
        }

        private static void DDZOption()
        {
            string operations;
            ClassDayDto classDay = new ClassDayDto();
            classDay.ClassDate = ConsoleInput.GetDateTime("Podaj date zajęć [dd/mm/yyyy]");
            foreach (var student in course.Students)
            {
                operations = ConsoleInput.GetYesNo("Czy " + student.Name + " " + student.Surname + " był obecny(a)? (tak/nie)");

                if (operations == "tak")
                {
                    classDay.Presense.Add(student, new PresenceDto(classDay,student,true));
                }
                if (operations == "nie")
                {
                    classDay.Presense.Add(student, new PresenceDto(classDay, student, false));
                }
            }

            course.AddClassDay(classDay);
        }

        private static void EventInfo(object sender, EventArgs e)
        {
            Console.WriteLine("Zdecydowales sie zapisac raport do pliku JSON");
        }
    }
}