﻿using System;
using System.Text.RegularExpressions;

namespace CourseLogicLayer
{
    public class Pesel
    {
        public string _peselNumber;

        public Pesel(string peselNumber)
        {
            if (!isValid(peselNumber) || peselNumber == null)
            {
                throw new Exception("Podano bledny pesel.");
            }
            _peselNumber = peselNumber;
        }

        private bool isValid(string peselNumber)
        {
            string pattern = @"^\d{11}$";
            var regex = new Regex(pattern);
            return regex.IsMatch(peselNumber);
        }

        public override string ToString()
        {
            return this._peselNumber;
        }
    }
}