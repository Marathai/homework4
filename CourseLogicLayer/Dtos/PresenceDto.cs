﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseLogicLayer.Dtos
{
    public class PresenceDto
    {
        public int Id { get; set; }
        public ClassDayDto classDay { get; set; }
        public StudentDto student { get; set; }
        public bool isPresent { get; set; }

        public PresenceDto(ClassDayDto classDay, StudentDto student, bool isPresent)
        {
            this.classDay = classDay;
            this.student = student;
            this.isPresent = isPresent;
        }

        public PresenceDto(int id, ClassDayDto classDay, StudentDto student, bool isPresent)
        {
            Id = id;
            this.classDay = classDay;
            this.student = student;
            this.isPresent = isPresent;
        }
    }
}
