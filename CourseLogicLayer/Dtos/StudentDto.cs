﻿using System;
using System.Collections.Generic;
using Homework1;
using Homework1.Services;

namespace CourseLogicLayer.Dtos
{
    public class StudentDto
    {
        public enum Gender
        {
            Male,
            Female,
            Other,
        }

        public int Id;
        public Pesel Pesel;
        public string Name;
        public string Surname;
        public DateTime Birthday;
        public Gender StudentGender;

        public List<CourseDto> Courses;
        public int DaysOfPresence = 0;
        public int TotalHomeworkPoints = 0;

        public StudentDto()
        {
            Courses = new List<CourseDto>();
        }

        public bool RemoveFromCourse(CourseDto course)
        {
            try { 
                return Courses.Remove(course);
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }
    }
}