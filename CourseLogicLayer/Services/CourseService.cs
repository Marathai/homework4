﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Repositories;
using Homework1.Dtos;
using Homework1.Mappers;

namespace Homework1.Services
{
    public class CourseService
    {
        private CourseDto currentCourseDto = null;

        public void AddCourse(CourseDto courseDto)
        {
            var course = FromDtoToEntityMapper.CourseDtoToEntity(courseDto);
            var courseRepo = new CourseRepository();
            courseRepo.AddCourse(course);
        }

        public List<CourseDto> GetAllCourses()
        {
            var repo = new CourseRepository();
            return repo
                .GetCourses()?
                .Select(c => FromEntityToDtoMapper.EntityToCourseDto(c))
                .ToList();
        }

        public void SetCurrentCourseByIndex(int index)
        {
            if (index < 0)
            {
                throw new Exception("Indeks nie moze byc liczba ujemna!");
            }

            var courses = GetAllCourses();
            if (courses.Count <= index)
            {
                throw new Exception("Nie ma takiego kursu!");
            }

            currentCourseDto = courses[index];
        }

        public CourseDto GetCurrentCourseDto()
        {
            return currentCourseDto;
        }
    }
}